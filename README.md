# Minecraft projects

ComputerCraft folder contains custom content for the ComputerCraft mod. Turtle programs and custom textures. The most important computercraft project is the quarry turtle program (2036 lines of lua).

CropMod is my own mod which adds a new dimension, items and blocks. CropMod is called CropMod because it adds 2 new kinds of seeds. Bone fruit seeds and coal flower seeds. These new seeds make farming coal and bonemeal possible in Minecraft 1.12.

ComputerCraft quarry program time-lapse:

![img](quarrytimelapse.gif)