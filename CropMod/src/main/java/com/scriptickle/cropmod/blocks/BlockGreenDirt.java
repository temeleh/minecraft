package com.scriptickle.cropmod.blocks;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.init.ModBlocks;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

import java.util.Objects;
import java.util.Random;

/**
 * Created by Scriptickle on 7:51 11.5.2018
 */
public class BlockGreenDirt extends Block implements IModelLoader {

    public BlockGreenDirt(String name) {
        super(Material.GRASS);
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
        this.setCreativeTab(CropMod.tabCropMod);
        this.setHardness(0.4f);
        this.setHarvestLevel("shovel", 0);
        this.setSoundType(SoundType.GROUND);

        ModBlocks.BLOCKS.add(this);
        ModItems.ITEMS.add(new ItemBlock(this).setRegistryName(Objects.requireNonNull(this.getRegistryName())));
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ModItems.GREEN_DUST;
    }

    @Override
    protected ItemStack getSilkTouchDrop(IBlockState state) {
        return new ItemStack(this, 1);
    }

    @Override
    public void registerModels() {
        CropMod.proxy.registerItemRenderer(Item.getItemFromBlock(this), 0, "inventory");
    }
}
