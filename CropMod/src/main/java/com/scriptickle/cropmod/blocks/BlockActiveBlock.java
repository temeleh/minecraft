package com.scriptickle.cropmod.blocks;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.init.ModBlocks;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

import java.util.Objects;

/**
 * Created by Scriptickle on 7:51 11.5.2018
 */
public class BlockActiveBlock extends Block implements IModelLoader {

    public BlockActiveBlock(String name) {
        super(Material.ROCK, MapColor.BLACK);
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
        this.setCreativeTab(CropMod.tabCropMod);
        this.setHardness(4.5f);
        this.setResistance(9.0f);
        this.setSoundType(SoundType.STONE);

        ModBlocks.BLOCKS.add(this);
        ModItems.ITEMS.add(new ItemBlock(this).setRegistryName(Objects.requireNonNull(this.getRegistryName())));
    }

    @Override
    public void registerModels() {
        CropMod.proxy.registerItemRenderer(Item.getItemFromBlock(this), 0, "inventory");
    }
}
