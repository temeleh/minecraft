package com.scriptickle.cropmod.blocks;


import com.scriptickle.cropmod.init.ModBlocks;
import com.scriptickle.cropmod.init.ModItems;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Objects;

/**
 * Created by Scriptickle on 11:17 11.5.2018
 */
public class BlockBonePlant extends BlockCrops {

    public BlockBonePlant(String name) {
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
        this.setLightLevel(0.4f);

        ModBlocks.BLOCKS.add(this);
        ModItems.ITEMS.add(new ItemBlock(this).setRegistryName(Objects.requireNonNull(this.getRegistryName())));
    }

    @Override
    public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state) {
        return super.canBlockStay(worldIn, pos, state) && !worldIn.getBlockState(pos.down()).getBlock().equals(Blocks.DIRT);
    }

    @Override
    protected Item getSeed() {
        return ModItems.BONE_SEEDS;
    }

    @Override
    protected Item getCrop() {
        return ModItems.BONE_CROP;
    }
}
