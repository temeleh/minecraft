package com.scriptickle.cropmod.world.gen;

import com.scriptickle.cropmod.util.Reference;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

/**
 * Created by Scriptickle on 19:46 14.5.2018
 */
public class WorldGenCustomStructures implements IWorldGenerator {

    public static final WorldGenStructure PLANTWORLD_HOUSE1 = new WorldGenStructure("plantworld_house1");
    public static final WorldGenStructure PLANTWORLD_HOUSE2 = new WorldGenStructure("plantworld_house2");
    public static final WorldGenStructure PLANTWORLD_WELL = new WorldGenStructure("plantworld_well");

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (world.provider.getDimension() == Reference.DIM_ID) {
            generateStructure(PLANTWORLD_HOUSE1, -5, -3, -5, world, random, chunkX, chunkZ, 60, 55, 245, Blocks.GRASS);
            generateStructure(PLANTWORLD_HOUSE2, -5, -3, -5, world, random, chunkX, chunkZ, 60, 55, 245, Blocks.GRASS);

            generateStructure(PLANTWORLD_WELL, -1, -28, -1, world, random, chunkX, chunkZ, 5, 45, 245, Blocks.GRASS);
        }
    }

    private void generateStructure(WorldGenerator generator, int xOffset, int yOffset, int zOffset, World world, Random random, int chunkX, int chunkZ, int chance, int minHeight, int maxHeight, Block topBlock) {
        int x = 8 + (chunkX * 16) + random.nextInt(15);
        int z = 8 + (chunkZ * 16) + random.nextInt(15);
        int y = calculateGenerationHeight(world, x, z, topBlock);

        BlockPos pos = new BlockPos(x, y, z);

        if (y > minHeight && y < maxHeight) {
            if (random.nextInt(chance) == 0) {
                generator.generate(world, random, pos.add(xOffset, yOffset, zOffset));
            }
        }
    }

    private static int calculateGenerationHeight(World world, int x, int z, Block topBlock) {
        int y = world.getHeight();
        boolean foundGround = false;

        while (!foundGround && y-- >= 0) {
            Block block = world.getBlockState(new BlockPos(x,y,z)).getBlock();
            foundGround = block == topBlock;
        }

        return y;
    }
}
