package com.scriptickle.cropmod.world.gen;

import com.scriptickle.cropmod.util.IStructure;
import com.scriptickle.cropmod.util.Reference;
import net.minecraft.block.state.IBlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraft.world.gen.structure.template.Template;
import net.minecraft.world.gen.structure.template.TemplateManager;

import javax.annotation.Nonnull;
import java.util.Random;

/**
 * Created by Scriptickle on 19:40 14.5.2018
 */
public class WorldGenStructure extends WorldGenerator implements IStructure {

    public String structureName;

    public WorldGenStructure(String name) {
        structureName = name;
    }

    @Override
    public boolean generate(@Nonnull World worldIn, @Nonnull Random rand, @Nonnull BlockPos position) {
        generateStructure(worldIn, position);
        return true;
    }

    public void generateStructure(World world, BlockPos pos) {
        MinecraftServer mcServer = world.getMinecraftServer();
        TemplateManager manager = worldServer.getStructureTemplateManager();
        Template template = manager.get(mcServer, new ResourceLocation(Reference.MODID, structureName));

        if (template != null) {
            IBlockState state = world.getBlockState(pos);
            world.notifyBlockUpdate(pos, state, state, 3);
            template.addBlocksToWorld(world, pos, settings);
        }
    }

}
