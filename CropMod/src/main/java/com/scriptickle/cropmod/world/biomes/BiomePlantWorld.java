package com.scriptickle.cropmod.world.biomes;

import com.scriptickle.cropmod.init.ModBlocks;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntitySkeletonHorse;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.BiomeJungle;

/**
 * Created by Scriptickle on 20:26 13.5.2018
 */
public class BiomePlantWorld extends BiomeJungle {

    public BiomePlantWorld() {
        super(false, new BiomeProperties("Plantworld").setHeightVariation(1f).setTemperature(0.95f).setBaseHeight(-1.99f).setRainDisabled().setWaterColor(6698343));

        fillerBlock = ModBlocks.GREEN_DIRT.getDefaultState();

        spawnableMonsterList.clear();
        spawnableCreatureList.clear();
        spawnableWaterCreatureList.clear();

        this.spawnableCreatureList.add(new SpawnListEntry(EntityBat.class, 100, 4, 4));
        this.spawnableCreatureList.add(new SpawnListEntry(EntitySkeletonHorse.class, 2, 4, 4));

        this.spawnableMonsterList.add(new SpawnListEntry(EntitySpider.class, 100, 4, 4));
        this.spawnableMonsterList.add(new SpawnListEntry(EntityCaveSpider.class, 10, 1, 1));
        this.spawnableMonsterList.add(new SpawnListEntry(EntitySkeleton.class, 100, 4, 4));
        this.spawnableMonsterList.add(new SpawnListEntry(EntityWitherSkeleton.class, 10, 1, 1));
        this.spawnableMonsterList.add(new SpawnListEntry(EntityCreeper.class, 100, 4, 4));
        this.spawnableMonsterList.add(new SpawnListEntry(EntityEnderman.class, 10, 4, 4));
        this.spawnableMonsterList.add(new SpawnListEntry(EntityWitch.class, 5, 1, 2));
    }

    @Override
    public int getGrassColorAtPos(BlockPos pos) {
        return 65373;
    }

    @Override
    public int getFoliageColorAtPos(BlockPos pos) {
        return 6698343;
    }
}
