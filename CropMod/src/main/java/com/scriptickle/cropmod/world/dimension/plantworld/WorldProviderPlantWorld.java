package com.scriptickle.cropmod.world.dimension.plantworld;

import com.scriptickle.cropmod.init.ModBiomes;
import com.scriptickle.cropmod.init.ModDimensions;
import com.scriptickle.cropmod.util.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.BiomeProviderSingle;
import net.minecraft.world.gen.IChunkGenerator;

/**
 * Created by Scriptickle on 19:58 13.5.2018
 */
public class WorldProviderPlantWorld extends WorldProvider {

    @Override
    protected void init() {
        this.biomeProvider = new BiomeProviderSingle(ModBiomes.PLANT_WORLD);
        setDimension(Reference.DIM_ID);
        this.hasSkyLight = true;
    }

    @Override
    public DimensionType getDimensionType() {
        return ModDimensions.PLANT_WORLD;
    }

    @Override
    public IChunkGenerator createChunkGenerator() {
        return new ChunkGeneratorPlantWorld(world);
    }

    @Override
    public double getMovementFactor() {
        return 64;
    }

    @Override
    public Vec3d getFogColor(float p_76562_1_, float p_76562_2_) {
        return new Vec3d(0,0.1,0);
    }

    @Override
    public Vec3d getSkyColor(Entity cameraEntity, float partialTicks) {
        return new Vec3d(0,0.8,0);
    }

    @Override
    public long getWorldTime() {
        return 18000;
    }

    @Override
    public boolean canRespawnHere() {
        return false;
    }

    @Override
    public WorldSleepResult canSleepAt(EntityPlayer player, BlockPos pos) {
        return WorldSleepResult.DENY;
    }

    @Override
    public boolean isSurfaceWorld() {
        return false;
    }

//    @Override
//    public float getSunBrightness(float par1) {
//        return 0;
//    }
//
//    @Override
//    public float getStarBrightness(float par1) {
//        return 0;
//    }

}
