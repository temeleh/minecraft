package com.scriptickle.cropmod;

import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.proxy.IProxy;
import com.scriptickle.cropmod.util.Reference;
import com.scriptickle.cropmod.util.EventHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Reference.MODID, name = Reference.NAME, version = Reference.VERSION)
public class CropMod {

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
    public static IProxy proxy;


    @Mod.EventHandler
    public static void preInit(FMLPreInitializationEvent event) {
        EventHandler.preInitRegistries();
    }


    public static CreativeTabs tabCropMod = new CreativeTabs("tab_cropmod") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(ModItems.ACTIVE_CROP);
        }
    };

}
