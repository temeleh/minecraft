package com.scriptickle.cropmod.init;

import com.scriptickle.cropmod.world.biomes.BiomePlantWorld;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

/**
 * Created by Scriptickle on 20:11 13.5.2018
 */
public class ModBiomes {

    public static final Biome PLANT_WORLD = new BiomePlantWorld();

    public static void registerBiomes() {
        initBiome(PLANT_WORLD, "Plantworld", Type.HILLS, Type.MOUNTAIN, Type.DENSE, Type.JUNGLE, Type.MAGICAL);
    }

    private static void initBiome(Biome biome, String name, Type... types) {
        biome.setRegistryName(name);
        ForgeRegistries.BIOMES.register(biome);
        BiomeDictionary.addTypes(biome, types);
    }
}
