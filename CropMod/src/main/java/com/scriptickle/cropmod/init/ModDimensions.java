package com.scriptickle.cropmod.init;

import com.scriptickle.cropmod.util.Reference;
import com.scriptickle.cropmod.world.dimension.plantworld.WorldProviderPlantWorld;
import net.minecraft.world.DimensionType;
import net.minecraftforge.common.DimensionManager;

/**
 * Created by Scriptickle on 20:02 13.5.2018
 */
public class ModDimensions {

    public static final DimensionType PLANT_WORLD = DimensionType.register("plantworld", "_plantworld", Reference.DIM_ID, WorldProviderPlantWorld.class, false);

    public static void registerDimensions() {
        DimensionManager.registerDimension(Reference.DIM_ID, PLANT_WORLD);
    }
}
