package com.scriptickle.cropmod.init;

import com.scriptickle.cropmod.items.*;
import com.scriptickle.cropmod.items.tools.*;
import com.scriptickle.cropmod.util.Reference;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.*;
import net.minecraftforge.common.util.EnumHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Scriptickle on 21:53 10.5.2018
 */
public class ModItems {

    public static final List<Item> ITEMS = new ArrayList<>();

    public static final Item ACTIVE_CROP = new ItemActiveCrop("active_crop");
    public static final Item BONE_CROP = new ItemBoneCrop("bone_crop");

    public static final Item ACTIVE_SEEDS = new ItemActiveSeeds("active_seeds");
    public static final Item BONE_SEEDS = new ItemBoneSeeds("bone_seeds");

    public static final Item PORTAL_OPENER = new ItemPortalOpener("portal_opener");

    public static final Item GREEN_DUST = new ItemGreenDust("green_dust");


    public static final Item ELYTRA_ARMOR_PLATE = new ItemElytraArmorPlate("elytra_armor_plate");

    public static final ItemArmor.ArmorMaterial ELYTRA_ARMOR_MATERIAL = EnumHelper.addArmorMaterial("elytra_armor_material", Reference.MODID + ":elytra", 40,
            new int[] {6, 9, 2, 5}, 17, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);

    public static final Item.ToolMaterial ELYTRA_TOOL_MATERIAL = EnumHelper.addToolMaterial("elytra_tool_material", 3, 1752, 13.0F, 3.5F, 17);

    public static final Item ELYTRA_HELMET = new ItemArmorBase("elytra_helmet", ELYTRA_ARMOR_MATERIAL, 1, EntityEquipmentSlot.HEAD);
    public static final Item ELYTRA_LEGGINGS = new ItemArmorBase("elytra_leggings", ELYTRA_ARMOR_MATERIAL, 2, EntityEquipmentSlot.LEGS);
    public static final Item ELYTRA_BOOTS = new ItemArmorBase("elytra_boots", ELYTRA_ARMOR_MATERIAL, 1, EntityEquipmentSlot.FEET);
    public static final Item ELYTRA_CHESTPLATE = new ItemArmorBase("elytra_chestplate", ELYTRA_ARMOR_MATERIAL, 1, EntityEquipmentSlot.CHEST);

    public static final Item ELYTRA_BOW = new ItemElytraBow("elytra_bow");

    public static final Item ELYTRA_SWORD = new ItemElytraSword("elytra_sword", ELYTRA_TOOL_MATERIAL);
    public static final Item ELYTRA_HOE = new ItemElytraHoe("elytra_hoe", ELYTRA_TOOL_MATERIAL);
    public static final Item ELYTRA_AXE = new ItemElytraAxe("elytra_axe", ELYTRA_TOOL_MATERIAL);
    public static final Item ELYTRA_PICKAXE = new ItemElytraPickaxe("elytra_pickaxe", ELYTRA_TOOL_MATERIAL);
    public static final Item ELYTRA_SPADE = new ItemElytraSpade("elytra_spade", ELYTRA_TOOL_MATERIAL);
}
