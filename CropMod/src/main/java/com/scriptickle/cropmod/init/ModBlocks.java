package com.scriptickle.cropmod.init;

import com.scriptickle.cropmod.blocks.*;
import net.minecraft.block.Block;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Scriptickle on 7:51 11.5.2018
 */
public class ModBlocks {

    public static final List<Block> BLOCKS = new ArrayList<>();

    public static final Block ACTIVE_BLOCK = new BlockActiveBlock("active_block");

    public static final Block ACTIVE_PLANT = new BlockActivePlant("active_plant");
    public static final Block BONE_PLANT = new BlockBonePlant("bone_plant");

    public static final Block PLANT_WORLD_PORTAL = new BlockPortalPlantWorld("plantworldportal");

    public static final Block GREEN_DIRT = new BlockGreenDirt("green_dirt");

}
