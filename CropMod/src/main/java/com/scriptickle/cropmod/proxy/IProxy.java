package com.scriptickle.cropmod.proxy;

import net.minecraft.item.Item;

/**
 * Created by Scriptickle on 22:04 10.5.2018
 */
public interface IProxy {
    void registerItemRenderer(Item item, int meta, String id);
}
