package com.scriptickle.cropmod.util;

import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.world.WorldServer;
import net.minecraft.world.gen.structure.template.PlacementSettings;
import net.minecraftforge.fml.common.FMLCommonHandler;

/**
 * Created by Scriptickle on 9:51 18.5.2018
 */
public interface IStructure {
    WorldServer worldServer = FMLCommonHandler.instance().getMinecraftServerInstance().getWorld(0);
    PlacementSettings settings = (new PlacementSettings()).setChunk(null).setIgnoreEntities(false).setIgnoreStructureBlock(false).setMirror(Mirror.NONE).setRotation(Rotation.NONE);
}
