package com.scriptickle.cropmod.util;

import com.scriptickle.cropmod.init.ModBiomes;
import com.scriptickle.cropmod.init.ModBlocks;
import com.scriptickle.cropmod.init.ModDimensions;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.world.gen.WorldGenCustomStructures;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Scriptickle on 22:27 10.5.2018
 */
@Mod.EventBusSubscriber
public class EventHandler {

    @SubscribeEvent
    public static void onItemRegister(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(ModItems.ITEMS.toArray(new Item[0]));
    }

    @SubscribeEvent
    public static void onBlockRegister(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(ModBlocks.BLOCKS.toArray(new Block[0]));
    }

    @SubscribeEvent
    public static void onModelRegister(ModelRegistryEvent event) {

        for (Item item : ModItems.ITEMS) {
            if (item instanceof IModelLoader) {
                ((IModelLoader)item).registerModels();
            }
        }

        for (Block block : ModBlocks.BLOCKS) {
            if (block instanceof IModelLoader) {
                ((IModelLoader) block).registerModels();
            }
        }

    }

    @SubscribeEvent
    public static void onFurnaceFuelCheck(FurnaceFuelBurnTimeEvent event) {
        if (event.getItemStack().getItem().equals(Item.getItemFromBlock(ModBlocks.ACTIVE_BLOCK))) {
            event.setBurnTime(12800);
        }
    }

    @SubscribeEvent
    public static void onElytraBowPullFov(FOVUpdateEvent event) {
        EntityPlayer p = event.getEntity();

        if (p.isHandActive() && p.getActiveItemStack().getItem() == ModItems.ELYTRA_BOW)
        {
            int i = p.getItemInUseMaxCount();
            float f1 = (float)i / 30.0F;

            if (f1 > 1.0F)
            {
                f1 = 1.0F;
            }
            else
            {
                f1 = f1 * f1;
            }

            float f = event.getFov();

            f *= 1.0F - f1 * 0.15F;

            event.setNewfov(f);
        }
    }

    public static void preInitRegistries() {
        GameRegistry.registerWorldGenerator(new WorldGenCustomStructures(), 0);

        ModBiomes.registerBiomes();

        ModDimensions.registerDimensions();

        LootTableList.register(new ResourceLocation(Reference.MODID, "plantworldloot"));
    }
}
