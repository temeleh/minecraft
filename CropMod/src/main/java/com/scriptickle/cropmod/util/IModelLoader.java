package com.scriptickle.cropmod.util;

/**
 * Created by Scriptickle on 22:07 10.5.2018
 */
public interface IModelLoader {
    void registerModels();
}
