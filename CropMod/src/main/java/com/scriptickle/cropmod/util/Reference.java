package com.scriptickle.cropmod.util;

/**
 * Created by Scriptickle on 21:35 10.5.2018
 */
public class Reference {
    public static final String MODID = "cropmod";
    public static final String NAME = "Crop Mod";
    public static final String VERSION = "2.0";

    public static final String CLIENT_PROXY_CLASS = "com.scriptickle.cropmod.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.scriptickle.cropmod.proxy.ServerProxy";

    public static final int DIM_ID = 3;

}
