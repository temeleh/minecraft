package com.scriptickle.cropmod.items.tools;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.item.ItemAxe;

/**
 * Created by Scriptickle on 19:21 29.9.2018
 */
public class ItemElytraAxe extends ItemAxe implements IModelLoader {

    public ItemElytraAxe(String name, ToolMaterial material)
    {
        super(material, 8.0F, -2.8F);
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(CropMod.tabCropMod);

        ModItems.ITEMS.add(this);
    }

    @Override
    public void registerModels()
    {
        CropMod.proxy.registerItemRenderer(this, 0, "inventory");
    }
}
