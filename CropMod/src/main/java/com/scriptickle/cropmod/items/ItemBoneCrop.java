package com.scriptickle.cropmod.items;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.item.ItemFood;

/**
 * Created by Scriptickle on 22:00 10.5.2018
 */
public class ItemBoneCrop extends ItemFood implements IModelLoader {

    public ItemBoneCrop(String name) {
        super(2, 0.3F, false);
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
        this.setCreativeTab(CropMod.tabCropMod);

        ModItems.ITEMS.add(this);
    }


    @Override
    public void registerModels() {
        CropMod.proxy.registerItemRenderer(this, 0, "inventory");
    }
}
