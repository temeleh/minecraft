package com.scriptickle.cropmod.items;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.blocks.BlockPortalPlantWorld;
import com.scriptickle.cropmod.init.ModBlocks;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Created by Scriptickle on 10:12 14.5.2018
 */
public class ItemPortalOpener extends ItemSword implements IModelLoader{
    public ItemPortalOpener(String name) {
        super(ToolMaterial.WOOD);
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
        this.setCreativeTab(CropMod.tabCropMod);

        ModItems.ITEMS.add(this);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitx, float hity, float hitz) {
        int x = pos.getX();
        int y = pos.getY();
        int z = pos.getZ();
        int side = facing.getIndex();

        if(side == 0) {
            y--;
        } else if(side == 1) {
            y++;
        } else if(side == 2) {
            z--;
        } else if (side == 3) {
            z++;
        } else if(side == 4) {
            x--;
        } else if(side == 5) {
            x++;
        }
        BlockPos adjacentAir = new BlockPos(x, y, z);

        if(!playerIn.canPlayerEdit(adjacentAir, facing, playerIn.getActiveItemStack())) {
            return EnumActionResult.FAIL;
        }
        IBlockState airBlock = worldIn.getBlockState(adjacentAir);
        IBlockState blockOnPos = worldIn.getBlockState(pos);

        if(airBlock.getBlock() == Blocks.AIR && (blockOnPos.getBlock() == Blocks.COAL_BLOCK || blockOnPos.getBlock() == ModBlocks.ACTIVE_BLOCK)) {
            worldIn.playSound(playerIn, adjacentAir, SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.BLOCKS, 1.0f, itemRand.nextFloat() * 0.4f + 0.8f);
            ((BlockPortalPlantWorld) ModBlocks.PLANT_WORLD_PORTAL).trySpawnPortal(worldIn, adjacentAir);

            playerIn.getHeldItem(hand).damageItem(5, playerIn);
        }
        return EnumActionResult.SUCCESS;
    }


    @Override
    public void registerModels() {
        CropMod.proxy.registerItemRenderer(this, 0, "inventory");
    }


}
