package com.scriptickle.cropmod.items.tools;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.item.ItemSword;

/**
 * Created by Scriptickle on 19:21 29.9.2018
 */
public class ItemElytraSword extends ItemSword implements IModelLoader {

    public ItemElytraSword(String name, ToolMaterial material)
    {
        super(material);
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(CropMod.tabCropMod);

        ModItems.ITEMS.add(this);
    }

    @Override
    public void registerModels()
    {
        CropMod.proxy.registerItemRenderer(this, 0, "inventory");
    }
}
