package com.scriptickle.cropmod.items;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.item.Item;

/**
 * Created by Scriptickle on 12:21 24.9.2018
 */
public class ItemElytraArmorPlate extends Item implements IModelLoader {

    public ItemElytraArmorPlate(String name) {
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
        this.setCreativeTab(CropMod.tabCropMod);

        ModItems.ITEMS.add(this);
    }

    @Override
    public void registerModels() {
        CropMod.proxy.registerItemRenderer(this, 0, "inventory");
    }
}
