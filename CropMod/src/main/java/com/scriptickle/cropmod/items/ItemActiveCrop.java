package com.scriptickle.cropmod.items;

import com.scriptickle.cropmod.CropMod;
import com.scriptickle.cropmod.init.ModItems;
import com.scriptickle.cropmod.util.IModelLoader;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Created by Scriptickle on 22:00 10.5.2018
 */
public class ItemActiveCrop extends Item implements IModelLoader {

    public ItemActiveCrop(String name) {
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
        this.setCreativeTab(CropMod.tabCropMod);

        ModItems.ITEMS.add(this);
    }

    @Override
    public int getItemBurnTime(ItemStack itemStack) {
        return 1280;
    }

    @Override
    public void registerModels() {
        CropMod.proxy.registerItemRenderer(this, 0, "inventory");
    }
}
