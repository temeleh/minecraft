---@diagnostic disable: cast-local-type
local args = { ... }


local dirs = {up = 0, forward = 1, down = 2}
local rots = {left = 0, right = 1, away = 2, back = 3}


local cloneCount = nil -- turtles placed by the host turtle are clones
local chestCount = 3 -- chests given to every turtle on init

-- settings
local length = 100                  --2 --blocks mined forward
local minHeight = 5                 --3 --minimum height the turtles will visit
local endHeight = 10                --4
local iterations = 3                --5 --count of 6 block tall areas dig by turtles
local onlyOres = false              --6 --mine selectively only ores from above and below
local fuelRequired = 20000          --7
local turtleCount = 32              --8 --clonecount + 1 --number of turtles in sync
local onlyValuable = false          --9 --only valuable ores with onlyOres (valuables from valuables table)


local isClone = false
local isDebug = false
local isRunning = false

local pos = nil
local startPos = nil
local rotation = rots.away

local askOptions = true

local turtlesPlaced = 0
local modemSide = nil


local hostId = 0
local protocol = nil





-----------------------------------------------------------------------------------------------------------------------
-- Setup
-----------------------------------------------------------------------------------------------------------------------

--[[
    Executes the movement function stepCount times.
    If movement() returns false then it executes the attack function until movement() returns true
]]
local function simpleMovement(stepCount, movement, attack)
    local c = 1
    while c <= stepCount do
        if movement() then
            c = c + 1
        else
            attack()
        end
    end
end


local function forward(stepCount) simpleMovement(stepCount, turtle.forward, turtle.attack) end
local function back(stepCount) simpleMovement(stepCount, turtle.back) end

local function left() turtle.turnLeft() end
local function right() turtle.turnRight() end

-- inspects a block and returns the name and metadata of the block
local function inspectNameUp() local _, data = turtle.inspectUp() return data.name, data.metadata end
local function inspectNameDown() local _, data = turtle.inspectDown() return data.name, data.metadata end
local function inspectName() local _, data = turtle.inspect() return data.name, data.metadata end

local function isChest(name)
    return (name == "minecraft:chest" 
        or name == "ae2:sky_stone_chest" 
        or name == "ae2:smooth_sky_stone_chest")
end

local function isTurtle(name)
    return (name == "computercraft:turtle" 
        or name == "computercraft:turtle_normal"
        or name == "computercraft:turtle_advanced")
end

local function isBucket(name)
    return name == "minecraft:bucket"
end

local function selectSlot(slot)
    if turtle.getSelectedSlot() ~= slot then turtle.select(slot) end
end

local function getSlotDetail(slot, leaveSlot)
    selectSlot(slot)
    local detail = turtle.getItemDetail()
    if detail == nil then return nil end

    if leaveSlot ~= nil then selectSlot(leaveSlot) end
    return detail.name, detail.count
end

local function getTypeItemCount(typeFunction)
    local total = 0
    local name, count

    for i = 1, 16 do
        if turtle.getItemCount(i) > 0 then
            name, count = getSlotDetail(i)

            if typeFunction(name) then
                total = total + count
            end
        end
    end

    selectSlot(1)
    return total
end

-- clears screen
local function clear()
    term.clear()
    term.setCursorPos(1, 1)
end

--[[
    Empties turtle's inventory (slots {fromSlot..16}) so if fromSlot is 1 the whole inventory is emptied

    Tries to drop items in the direction dir (default down). If the items cannot be dropped then returns false.
    Selects slot 1 after successfully emptying inventory.
]]
local function emptyInventory(fromSlot, dir)
    if dir == nil then dir = dirs.down end

    for i = fromSlot, 16 do
        if turtle.getItemCount(i) > 0 then
            selectSlot(i)

            if dir == dirs.forward then
                if not turtle.drop() then return false end
            elseif dir == dirs.up then
                if not turtle.dropUp() then return false end
            elseif dir == dirs.down then
                if not turtle.dropDown() then return false end
            end
        end
    end
    selectSlot(1)
    return true
end

local function isEmptyInventory(fromSlot)
    for i = fromSlot, 16 do
        if turtle.getItemCount(i) > 0 then
            return false
        end
    end
    return true
end

local function clearLine()
    local x, y = term.getCursorPos()

    term.setCursorPos(1, y)
    term.clearLine()
end

-- maps numbers 0-9 to keys zero-nine and numPad0-numPad9
local function keyToNumber(key)
    if key == keys.zero or key == keys.numPad0 then return 0 end
    if key == keys.one or key == keys.numPad1 then return 1 end
    if key == keys.two or key == keys.numPad2 then return 2 end
    if key == keys.three or key == keys.numPad3 then return 3 end
    if key == keys.four or key == keys.numPad4 then return 4 end
    if key == keys.five or key == keys.numPad5 then return 5 end
    if key == keys.six or key == keys.numPad6 then return 6 end
    if key == keys.seven or key == keys.numPad7 then return 7 end
    if key == keys.eight or key == keys.numPad8 then return 8 end
    if key == keys.nine or key == keys.numPad9 then return 9 end
    return nil
end

--[[
    Prints a question string, waits for user input and returns the selected value
    The input is an integer and it can be selected incrementally with arrow keys
    or by typing a number directly (typing only when arrowIncrement == 1)

    arrowIncrement controls how many times (by how much) the number is incremented when an arrow key is pressed.
]]
local function askQuestion(question, min, max, default, arrowIncrement)
    clear()
    print(question .. "\n")

    local num = default
    local concatted = nil
    while true do
        write("<< " .. num .. " >>")

        local event, key = os.pullEvent("key")

        if key == keys.enter then break end

        if key == keys.left and (num - arrowIncrement >= min) then num = num - arrowIncrement concatted = nil end
        if key == keys.right and (num + arrowIncrement <= max) then num = num + arrowIncrement concatted = nil end

        if arrowIncrement == 1 then
            local n = keyToNumber(key)
            if n ~= nil then
                if concatted == nil then
                    concatted = n
                else
                    concatted = tonumber(concatted .. n)
                end

                if concatted >= min and concatted <= max then num = concatted end

                if concatted > max then
                    concatted = n

                    if concatted >= min and concatted <= max then num = concatted end
                end
            end
        end

        clearLine()
    end
    return num
end

--[[
    Prints an info string and waits for confirmation. 
    If yesNo == true then info is a yes/no question which requires yes/no input from the user
    
    If not an yes/no question then pressing enter confirms.
    continueText is a custom "Press enter to continue" text
]]
local function confirm(info, yesNo, continueText, askContinue)
    clear()
    print(info .. "\n")

    if askContinue == nil then askContinue = true end

    if yesNo then
        print("Yes: y or enter\nNo: any other key")
        local event, key = os.pullEvent("key")
        return key == keys.enter or key == keys.y
    elseif askContinue then
        if continueText == nil then continueText = "Press enter to continue" end
        print(continueText)
        while true do
            local event, key = os.pullEvent("key")
            if key == keys.enter then break end
        end
    else
        print("Continuing in 5 seconds...")
        sleep(5)
    end
end

--[[
    Tries to open Rednet.
    If turtle doesn't have a modem then returns false
]]
local function openRednet()

    -- Find a modem (code taken from gps api)
    local sModemSide = nil
    for n,sSide in ipairs(rs.getSides()) do
        if peripheral.getType(sSide) == "modem" then
            sModemSide = sSide
            break
        end
    end

    if sModemSide == nil then
        print("No wireless modem attached")
        return false
    end

    modemSide = sModemSide

    rednet.open(modemSide)
    return true
end

--[[
    Updates the global pos vector variable with the turtle's position from gps
    If gps.locate cannot locate turtle then returns false and the pos vector is reset to (nil,nil,nil)
]]
local function updatePos()
    local x, y, z = gps.locate()

    if x == nil then
        print("Could not get turtle position!\nCheck that turtle is in range of a gps host")
        pos = vector.new(nil, nil, nil)
        return false
    end

    pos = vector.new(x, y, z)
    return true
end









-- Ask all required parameters from the user
local function askParameters()
    -- Number of turtles
    turtleCount = askQuestion("How many turtles? (including this one)\n\nSelect a value with left and right arrows or type a number.\n\nPress enter to continue", 1, 200, turtleCount, 1)
    cloneCount = turtleCount - 1

    -- Minimum height and calculated maximum height
    minHeight = askQuestion("What is the minimum height the turtles will go?", 5, 200, minHeight, 1)

    local maxIterations = math.floor((255 - (minHeight + 6)) / 6) -- Maximum number of iterations

    local minmax = minHeight + 3 -- Minimum allowed maximum height
    local maxmax = minmax + (maxIterations * 6) -- Maximum allowed maximum height

    local commonSettings = settings.get("COMMON") -- getting iterations from the previous run settings
    if commonSettings == nil or commonSettings[5] == nil then 
        iterations = 3
    else
        iterations = commonSettings[5]
    end

    if iterations > maxIterations then 
        iterations = maxIterations
    end

    local default = minmax + ((iterations - 1) * 6)

    local maxHeight = 3 + askQuestion("What is the maximum height the turtles will go?\n\n" ..
                                "Possible heights: " .. minHeight .. " + n * 6,\nwhere 1 <= n <= " .. maxIterations, minmax, maxmax, default, 6)

    iterations = math.floor((maxHeight - minHeight - 3) / 6) + 1
    length = askQuestion("How many blocks forward turtles should mine?", 5, 1000, length, 1)


    -- Clear the whole area?
    onlyOres = confirm("Target only ores?\n\nIf yes, only every third layer will be mined completely.", true)

    if onlyOres then
        onlyValuable = confirm("Target only VALUABLE ores?\n\nIf yes, only valuable ores (diamond, gold, quartz and iron) will be targeted.", true)
    end


    clear()
end

-- Prints a info screen of the selected parameters and other useful info before starting the quarry
local function printStartInfo()
    local levels = iterations * 2
    local maxHeight = (minHeight + iterations * 6)

    endHeight = maxHeight - 3

    fuelRequired = levels * length + iterations * 6 + (pos.y - maxHeight) + 500

    local oresS = "every third layer" if not onlyOres then oresS = "clear whole area" end

    local info = "Total area: " .. turtleCount .. "x" .. (length + 1) .. "x" .. ((endHeight + 2) - (minHeight - 1)) .. " (WxLxH)\n\n" ..
           "Maximum height: " .. endHeight .. "\n" ..
           iterations .. " iterations, " .. oresS .. "\n\n" ..
           "Required fuel estimate:\n    One turtle: " .. fuelRequired .. "\n    Total: " .. (fuelRequired * turtleCount) .. "\n"

    confirm(info, false, nil, askOptions)

    clear()
    print("...")
end










-- placing clones ---------------------------------------------------------------------------------------------------


-- empties the chest in front 
local function emptyChest(fromSlot)
    selectSlot(fromSlot)
    while turtle.suck() do
        emptyInventory(fromSlot)
    end
end


--[[
    Helper to suck non-stackable items from a chest. 
    If there are not enough items, function sleeps 0.5 second intervals until there are more items in the chest.
]]
local function suckNonStackable(count)
    local slot = 1

    while slot <= count and slot <= 16 do
        selectSlot(slot)
        if not turtle.suck(1) then
            clear()
            print("Not enough items in chest! Waiting for more items...")
            while not turtle.suck(1) do
                sleep(0.5)
            end
            slot = slot + 1
        else
            slot = slot + 1
        end
    end
    selectSlot(1)
end


--[[
    Loads items from the user. Prints a info text and checks if the user placed the items correctly.

    numberOfItems -- how many items
    nonStackable -- true if cannot get all items with a single turtle.suck() call
    slot -- slot where the new items are primarily put
    leaveBehind -- how many items have to be in the slot after this function executes
]]
local function loadItems(info, numberOfItems, nonStackable, slot, leaveBehind)
    while true do
        confirm(info)
        clear()

        local stillNeedChest = false

        if cloneCount > 16 and nonStackable then
            if not isChest(inspectName()) then stillNeedChest = true end
        end

        if stillNeedChest then
            print("You didn't place a chest in front of the turtle!")
            sleep(4)
        elseif isEmptyInventory(slot) then
            if nonStackable then
                suckNonStackable(numberOfItems)
            else
                turtle.suck(numberOfItems)
            end

            if turtle.getItemCount() == 0 then
                print("Could not find enough new items!\n\nWaiting...")
                os.pullEvent("turtle_inventory")
            else break end
        else
            if not isEmptyInventory(slot) then
                break
            else
                print("Could not find enough new items!\n\nWaiting...")
                os.pullEvent("turtle_inventory")
            end
        end
    end
end

-- Places a clone from above so that it is oriented properly (forward)
local function placeFirstCloneDifferent()
    turtle.dig()
    turtle.digUp()
    turtle.up()
    turtle.dig()
    turtle.forward()

    turtle.placeDown()
    turtlesPlaced = turtlesPlaced + 1
    turtle.back()

    turtle.down()

    if cloneCount > 1 then
        turtle.back()
    else
        right()
    end
end

--[[
    Places all the clones from the turtle's inventory and returns to its starting position
]]
local function placeClonesFromInventory()
    left()
    forward(cloneCount - 1 - turtlesPlaced)
    if turtlesPlaced == 0 then placeFirstCloneDifferent() end

    local slot = 1

    while slot <= 16 and turtlesPlaced < cloneCount do
        if turtle.getItemCount(slot) == 0 then
            slot = slot + 1
        else
            selectSlot(slot)
            while not turtle.place() do turtle.attack() end

            turtlesPlaced = turtlesPlaced + 1
            if turtlesPlaced < cloneCount then back(1) else right() end
        end
    end
    selectSlot(1)
end

--[[
    Places all the available clones (cloneCount) from the chest in front.
    Turtle returns to its initial position but oriented towards the mining area ("left")
]]
local function placeClones()
    emptyInventory(1)

    local countInfo = cloneCount .. " turtles in the turtle's inventory"

    if cloneCount > 16 then countInfo = "16 turtles in the turtle's inventory and " .. (cloneCount - 16) .. " turtles in a chest in front of the turtle" end

    loadItems("Put " .. countInfo .. ".\n\n" ..
        "These turtles should be initialized\nwith 'quarry init <protocol>' at least once", cloneCount, true, 1, 1)

    clear()
    print("Placing turtles...")

    while true do
        placeClonesFromInventory()
        emptyInventory(1)

        if turtlesPlaced < cloneCount then
            back(cloneCount - turtlesPlaced - 1)
            right()
            suckNonStackable(cloneCount - turtlesPlaced)
        else return end
    end

    emptyChest(1)
end










-- helpers for settings and communication -------------------------------------------------------------------------------------


--[[
    Sets and saves a setting.
    If dontSend is false then broadcasts the setting with the quarry protocol
]]
local function saveSetting(setting, value, dontSend)
    settings.set(setting, value)
    settings.save(".settings")

    if not dontSend then rednet.broadcast({setting, value}, protocol) end
end

--[[
    Sets the given settingsData table in the .settings file with a key "COMMON".
    Settings are not loaded.

    The old .settings file is deleted
]]
local function setCommonSettings(settingsData)
    fs.delete(".settings")

    saveSetting("COMMON", settingsData, true)
end

--[[
    Saves and returns the current common settings
]]
local function saveCommonSettings()
    local settingsData = {"SETTINGS", length, minHeight, endHeight, iterations, onlyOres, fuelRequired, turtleCount, onlyValuable}
    setCommonSettings(settingsData)

    return settingsData
end


--[[
    Loads the settingsData table into memory (sets the corresponding variables).
    If settingsData is nil then it is read from the .settings file.
]]
local function loadCommonSettings(settingsData)
    if settingsData == nil then settingsData = settings.get("COMMON") end
    if settingsData == nil then return end

    length = settingsData[2]
    minHeight = settingsData[3]
    endHeight = settingsData[4]
    iterations = settingsData[5]
    onlyOres = settingsData[6]
    fuelRequired = settingsData[7]
    turtleCount = settingsData[8]
    onlyValuable = settingsData[9]
end

--[[
    Sends a confirmation to the host computer. The confirmation message is the turtle's fuel level 
    
    info -- (optional) info text printed in this computer
]]
local function sendConfirmation(info)
    clear()
    if info ~= nil then print(info) end

    print("Sending confirmation to host...")
    rednet.send(hostId, turtle.getFuelLevel(), protocol)
end

--[[
    Broadcasts data table and waits for neededResponses number of responses.
    data is a lua table with a tag string as the first element. Example: {"TAG", variable1, variable2, ...}

    waitInfo -- (optional) custom "Waiting for responses..." string
]]
local function informAndWait(data, neededResponses, waitInfo)
    rednet.broadcast(data, protocol)

    if waitInfo == nil then waitInfo = "Waiting for responses..." end

    if neededResponses > 0 then clear() print(waitInfo .. "\n") end

    for i = 1, neededResponses do
        local id, fuel = rednet.receive(protocol)
        if i == 1 then print("Responses: ") end

        print("    id:" .. id .. " fuel: " .. fuel .. "/" .. fuelRequired)
    end
end

local function inform(data)
    informAndWait(data, 0)
end


--[[
    Waits for a data table from a computer with an id fromId and the given tag.
    Tag is the first element in the data table.
]]
local function receiveInform(tag, fromId)
    local id, data = rednet.receive(protocol)
    while id ~= fromId or data[1] ~= tag do
        id, data = rednet.receive(protocol)
    end
    return data
end












-- init clones ---------------------------------------------------------------------------------------------


--[[
    Toggles redstone output on side (default "front")
]]
local function sendRsSignal(side)
    if side == nil then side = "front" end
    rs.setOutput(side, not rs.getOutput(side))
end

--[[
    Waits until receiving any redstone signal
]]
local function waitForRsSignal()
    os.pullEvent("redstone")
end






--[[
    Drops specific type items so that leaveBehind number of items are left in the leaveSlot slot.

    All items are dropped forwards, so dropSpecific calls can be chained between turtles to distribute items among all turtles.
]]
local function dropSpecific(typeFunction, leaveBehind, leaveSlot)
    local count, name, fewSlot = nil, nil, nil

    for i = leaveSlot, 16 do
        count = turtle.getItemCount(i)

        if count > 0 then
            name = getSlotDetail(i)

            if typeFunction(name) then
                if count <= leaveBehind then
                    leaveBehind = leaveBehind - count
                    fewSlot = i
                else
                    turtle.drop(count - leaveBehind)

                    leaveBehind = 0
    
                    if fewSlot ~= nil then
                        turtle.transferTo(fewSlot)
                    else
                        fewSlot = i
                    end
                end
            end
        end
    end
    
    selectSlot(fewSlot)
    turtle.transferTo(leaveSlot)
    selectSlot(1)
end

--[[
    Distributes a specific item type among all turtles.
    Items are loaded from the user with loadItems (if needed)
]]
local function distributeItem(typeFunction, info, slot, leaveBehind, numberOfItems)
    if getTypeItemCount(typeFunction) < numberOfItems then
        loadItems(info, numberOfItems, false, slot, leaveBehind)
    end

    left()
    dropSpecific(typeFunction, leaveBehind, slot)
    sendRsSignal()
    right()

    informAndWait({"RECEIVE ITEM", slot, leaveBehind}, cloneCount)
end

--[[
    Asks the buckets and chests from the user and distributes them among turtles 
    so that every turtle has one bucket and chestCount chests.
]]
local function distributeBucketChests()
    distributeItem(isBucket, "Next, put " .. turtleCount .. " empty buckets in the turtle's inventory.", 1, 1, turtleCount)

    local totalChestCount = turtleCount * chestCount
    distributeItem(isChest, "Finally, put " .. totalChestCount .. " chests in the turtle's inventory.", 2, chestCount, totalChestCount)
end


local function clearTrashFromInventory()
    local bucket, bc = getSlotDetail(1)
    local chests, cc = getSlotDetail(2)

    if isBucket(bucket) and bc == 1 and isChest(chests) then
        emptyInventory(3)
        turtle.transferTo(3)
        return
    end

    emptyInventory(1)
end





--[[
    Distributes fuel among all turtles
]]
local function distributeFuel()
    while true do
        sleep(0.3)
        if turtle.getItemCount(1) > 0 then
            turtle.drop(1)
            sendRsSignal()
        end
    end
end

local clonesNeedingFuel = 0

--[[
    Wait until all clones have enough fuel
]]
local function waitUntilClonesHaveFuel()
    local infoText = "Now, place fuel in the first slot of this turtle.\n\n" ..
        "The fuel will be distributed among all turtles"

    -- Saves and sends settings
    informAndWait(saveCommonSettings(), clonesNeedingFuel, infoText)

    sleep(0.5)
    inform({"STOP FUELING"})
    sleep(0.5)
end


--[[
    Gives quarry settings and fuel to the clone turtles and waits until every turtle has enough fuel
]]
local function initCloneSettingsAndFuel()
    clear()

    left()
    parallel.waitForAny(distributeFuel, waitUntilClonesHaveFuel)

    right()
    rs.setOutput("front", false)
    
    selectSlot(3)
    turtle.transferTo(1)
    selectSlot(1) 
end









local function listenCloneFuelResponses()
    cloneCount = 0

    while true do
        local cloneId, cloneFuel = rednet.receive(protocol)

        print("    id:" .. cloneId .. " fuel: " .. cloneFuel .. "/" .. fuelRequired)

        cloneCount = cloneCount + 1
        if cloneFuel < fuelRequired then 
            clonesNeedingFuel = clonesNeedingFuel + 1 
        end
    end
end

local function sleepFewSeconds()
    sleep(5)
    print("Found "..cloneCount.." clones!")
    turtleCount = cloneCount + 1
    sleep(5)
end




--[[
    Turns on the clone turtles and receives their fuel levels (counts how many turtles need fuel).
]]
local function turnOnClones()
    clear()

    print("Turning on the turtles...\n\nTurtles should start running\n'quarry clone'.\n")
    print("Waiting for responses from the clone turtles...\n")

    print("Responses: ")

    -- reboot other computers
    -- turnOn is used here because computer.reboot() doesn't turn on computers that are powered off
    left()
    peripheral.call("front", "reboot") 
    peripheral.call("front", "turnOn")
    right()

    parallel.waitForAny(listenCloneFuelResponses, sleepFewSeconds)
end



-----------------------------------------------------------------------------------------------------------------------






--[[
    Asks the user to put fuel into slot 1 if fuel level is less than given fuelAmount.
    Loops until turtle has enough fuel. Excess fuel is dropped down.
]]
local function requestFuel(fuelAmount)
    while turtle.getFuelLevel() < fuelAmount do
        clear()
        print("This computer doesn't have enough fuel.\nPut fuel in slot 1\n\nFuel level: " .. turtle.getFuelLevel() .. "/" .. fuelRequired)

        if not turtle.refuel(3) then
            os.pullEvent("turtle_inventory")
        end
    end
    turtle.dropDown()
end



--[[
    Checks if chests are already placed behind the host computer and if clone turtles are placed
    If there is a chest behind the turtle then sets chestCount accordingly.

    Returns true if there is a turtle (or advanced turtle) left side of the turtle, else false
]]
local function checkSurroundings()
    print("...")

    local chests = false
    local turtles = false

    left() 
    if isTurtle(inspectName()) then
        turtles = true
    end
    left()

    if isChest(inspectName()) then
        chests = true

    elseif turtle.forward() then 
        chests = isChest(inspectName())
            
        turtle.back()
    end

    right() right()

    if chests then
        chestCount = 1
    else
        chestCount = math.ceil((iterations * 13) / 36)
        if chestCount <= 2 then chestCount = 3 end
    end

    clear()
    return turtles
end

--[[
    Initializes the host turtle. 
    Loads parameters, places and sets up clone turtles and turns on the quarry after user confirmation.
]]
local function initHost()
    loadCommonSettings()

    if askOptions then askParameters() end
    
    local areTurtlesPlaced = checkSurroundings()


    printStartInfo()

    clearTrashFromInventory()

    if turtle.getFuelLevel() < fuelRequired then
        requestFuel(fuelRequired)
    end

    rednet.host(protocol, "host")
    hostId = os.getComputerID()

    if cloneCount > 0 then
        if not areTurtlesPlaced then placeClones() end

        turnOnClones()
        initCloneSettingsAndFuel()

        informAndWait({"SYNC INVENTORY"}, cloneCount, "Waiting for turtles to give their items to the host")
    else
        saveCommonSettings()

        selectSlot(3)
        turtle.transferTo(1)
        selectSlot(1) 
    end

    distributeBucketChests()

    rs.setOutput("front", false)
    clear()

    confirm("Setup completed", false, "PRESS ENTER TO START THE QUARRY", askOptions)

    inform({"START QUARRY"}) -- don't wait for confirmation
    clear()
end










-----------------------------------------------------------------------------------------------------------------------
-- Setup clones
-----------------------------------------------------------------------------------------------------------------------


--[[
    A loop to refuel received fuel or give it to the next turtle.
]]
local function moveFuel()
    while true do
        waitForRsSignal()

        if turtle.getFuelLevel() < fuelRequired then
            -- Uses the fuel
            if not turtle.refuel() then
                turtle.dropDown()
            else
                if turtle.getFuelLevel() >= fuelRequired then
                    sendConfirmation("Enough fuel")
                end
            end
        else
            -- Drops the fuel for the next turtle and sends a rs signal
            turtle.drop()
            sendRsSignal()
        end
    end
end

--[[
    Blocks until receives STOP FUELING message from the host (frees slot 1 for refueling)
]]
local function waitForMovingFuel()
    receiveInform("STOP FUELING", hostId)

    selectSlot(3)
    turtle.transferTo(1)
    selectSlot(1)
end


local function moveItems()
    receiveInform("SYNC INVENTORY", hostId)

    local isLastTurtle = not isTurtle(inspectName())

    right()

    if not isLastTurtle then
        waitForRsSignal()
    end

    right() -- turn180

    while not emptyInventory(1, dirs.forward) do print("Couldn't give items") end

    sendRsSignal()

    sendConfirmation("Inventory empty")

    left() 
    rs.setOutput("front", false)
    left()
end



--[[
    Initializes this clone.
]]
local function initClone()
    -- Starting up other turtles --
    peripheral.call("front", "reboot")
    peripheral.call("front", "turnOn") -- (if powered off)

    hostId = rednet.lookup(protocol, "host")
    if hostId == nil then
        print("No active host!\n")
        rednet.close(modemSide)
        return false
    end

    sleep(3) -- Wait for the host to turn back after turning clones on
    print("Sending fuel level to the host...")
    rednet.send(hostId, turtle.getFuelLevel(), protocol)

    -- Wait for settings from host
    local data = receiveInform("SETTINGS", hostId)
    clear()

    setCommonSettings(data)
    loadCommonSettings(data)

    clearTrashFromInventory()
    
    -- Fueling
    parallel.waitForAny(moveFuel, waitForMovingFuel)

    -- Give bucket and chests (from the previous run) to the host
    moveItems()

    -- Receiving a bucket and chests --
    for i = 1, 2 do
        data = receiveInform("RECEIVE ITEM", hostId)
        clear()

        local slot, leaveBehind = data[2], data[3]

        -- wait until receiving item
        print("Waiting for items...\n")
        waitForRsSignal()

        if i == 1 then 
            dropSpecific(isBucket, leaveBehind, slot)
        else 
            dropSpecific(isChest, leaveBehind, slot) 
        end

        sendRsSignal()
        sendConfirmation("Item Received")
    end
    
    clear()

    -- Wait for permission to start the quarry
    receiveInform("START QUARRY", hostId)

    right()

    return true
end



-----------------------------------------------------------------------------------------------------------------------
-- Init
-----------------------------------------------------------------------------------------------------------------------

local function init(protocol)
    -- Write startup file
    local file = fs.open("startup", "w")
    file.write("shell.run('quarry clone')")
    file.close()

    print("Startup file initialized")

    os.setComputerLabel("q_" .. os.getComputerID())
    print("Label set to: " .. os.getComputerLabel() .. "\n")

    print("Protocol: " .. protocol)

    -- Write protocol to a file
    local pfile = fs.open(".protocol", "w")
    pfile.write(protocol)
    pfile.close()
end

local function initAll(protocol)
    local success, data = turtle.inspect()

    if data.name ~= "computercraft:peripheral" or data.state.variant ~= "disk_drive_full" or not disk.hasData("front") then
        print("Place a disk drive with a floppy disk in front of the turtle and run\n'quarry initall <protocol>' again")
        return
    end

    print("Initializing turtles with quarry init " .. protocol .. "...")

    local file = fs.open("disk/startup", "w")
    file.write("shell.run('quarry init " .. protocol .. "')")
    file.close()

    if not turtle.up() then
        print("Cannot move up!!\n\nDoes the turtle have fuel?")
        return
    end

    for i = 1, 16 do
        if turtle.getItemCount(i) > 0 then
            if isTurtle(getSlotDetail(i)) then
                turtle.place()

                peripheral.call("front", "turnOn")

                turtle.dig()
            end
        end
    end

    turtle.down()
    selectSlot(1)
end



-----------------------------------------------------------------------------------------------------------------------
-- Main
-----------------------------------------------------------------------------------------------------------------------




-- Parse command line arguments
if #args == 1 then
    if args[1] == "clone" then
        isClone = true
        if settings.get("RUNNING") then isRunning = true end

    elseif args[1] == "debug" then
        isDebug = true
    elseif args[1] == "init" then
        print("Usage: quarry init <protocol>")
        return
    elseif args[1] == "initall" then
        print("Usage: quarry initall <protocol>")
        return
    elseif args[1] == "resume" then
        isClone = true
        isRunning = true
    elseif args[1] == "sidechange" then
        turtle.up()
        right() right()
        forward(2)
        turtle.down()

        return
    elseif args[1] == "previous" then
        print("Using previous settings")
        sleep(1)
        askOptions = false
    end
elseif #args == 2 then
    if args[1] == "init" then
        init(args[2])
        return
    elseif args[1] == "initall" then
        initAll(args[2])
        return
    end
elseif #args == 5 then
    turtleCount = tonumber(args[1])
    minHeight = tonumber(args[2])
    iterations = tonumber(args[3])
    length = tonumber(args[4])

    if turtleCount == nil or minHeight == nil or iterations == nil or length == nil then
        print("Usage: quarry <turtleCount> <minHeight> <iterations> <length> {all/ores/valuable}")
        return
    end

    if args[5] ~= "all" then
        if args[5] == "ores" then
            onlyOres = true
        elseif args[5] == "valuable" then
            onlyOres = true
            onlyValuable = true
        else
            print("Unknown option: " .. args[5])
            print("Usage: quarry <turtleCount> <minHeight> <iterations> <length> {all/ores/valuable}")
            return
        end
    end

    askOptions = false
elseif #args ~= 0 then
    print("Usage: quarry\nor")
    print("Usage: quarry previous\nor")
    print("Usage: quarry <turtleCount> <minHeight> <iterations> <length> {all/ores/valuable}\nor")
    print("Usage: quarry init <protocol>\nor")
    print("Usage: quarry initall <protocol>")
    return
end





-- Get protocol
local pfile = fs.open(".protocol", "r")
if pfile == nil then
    print("No protocol specified!\n\nRun 'quarry init <protocol>' first")
    return
end

protocol = pfile.readLine()
pfile.close()


if isRunning then sleep(5) end

if not openRednet() then return end
if not updatePos() then return end


startPos = {x = pos.x, y = pos.y, z = pos.z}


if not isClone and not isDebug then -- when host is resuming, it is a clone, so don't need to check isRunning 
    initHost() 
end


if isClone and not isRunning then
    if not initClone() then
        return
    end
end


-----------------------------------------------------------------------------------------------------------------------
-- Quarry
-----------------------------------------------------------------------------------------------------------------------



--[[
    Table that keeps track of types of items in different slots.
    First slot always has a bucket and second slot has chests.
]]
local slotTypes = {
--[["minecraft:bucket", "minecraft:chest",]]"empty",            "empty",
    "empty",            "empty",            "empty",            "empty",
    "empty",            "empty",            "empty",            "empty",
    "empty",            "empty",            "empty",            "empty",
}


local currentLevel = 0
local isUnloading = false


local lava = "minecraft:lava"
local lavaFlow = "minecraft:flowing_lava"
local water = "minecraft:water"
local waterFlow = "minecraft:flowing_water"



--[[
    Tables of all supported mined item types

    Number value is a typeValue that defines how valuable an item is.
    Lower valued items are thrown away sooner if inventory is full.

    unknown is a type for all other types not listed here 
    and chestloot type is given to items from underground chests.
]]
local ground = {
    ["minecraft:dirt"] = 0,
    ["minecraft:cobblestone"] = 0,
    ["minecraft:gravel"] = 1,
    ["minecraft:obsidian"] = 1,
    ["unknown"] = 1,
    ["granite"] = 2,
    ["diorite"] = 2,
    ["andesite"] = 2,
    ["minecraft:sand"] = 3,
    ["minecraft:log"] = 3,
    ["minecraft:flint"] = 3
}

local ores = {
    ["minecraft:coal_ore"] = 4,
    ["minecraft:redstone_ore"] = 4,
    ["minecraft:lapis_ore"] = 4,
    ["minecraft:emerald_ore"] = 5
}

local valuables = {
    ["minecraft:quartz_ore"] = 6,
    ["minecraft:iron_ore"] = 6,
    ["chestloot"] = 7,
    -- ["ae2:charged_quartz_ore"] = 8,
    ["minecraft:gold_ore"] = 9,
    -- ["ae2:quartz_ore"] = 10,
    ["minecraft:diamond_ore"] = 11
}

--[[
    Turns 180 degrees and saves the current rotation in settings.
]]
local function turn180()
    if rotation == rots.away then
        rotation = rots.left
        saveSetting("ROTATION", rotation)
        turtle.turnLeft() turtle.turnLeft()
        rotation = rots.back
        saveSetting("ROTATION", rotation)

    elseif rotation == rots.back then
        rotation = rots.right
        saveSetting("ROTATION", rotation)
        turtle.turnLeft() turtle.turnLeft()
        rotation = rots.away
        saveSetting("ROTATION", rotation)
    end
end

local function isFullInventory(fullCount)
    return turtle.getItemCount(16) > 0 or (fullCount > 0 and turtle.getItemCount(15) > 0)
end

--[[
    Returns true if the name is in the valuables table.
    If onlyValuable == false then item is considered valuable also if it is in the ores table.
]]
local function isValuable(name)
    return valuables[name] ~= nil or (not onlyValuable and ores[name] ~= nil)
end

local function isOre(name) -- valuables are also ores
    return valuables[name] ~= nil or ores[name] ~= nil
end

local function isLiquid(name)
    return name == lava or name == water or name == lavaFlow or name == waterFlow
end

local function isLavaSource(name, metadata)
    return (name == lava or name == lavaFlow) and metadata == 0
end

--[[
    Returns how many slots are filled with valuable ores
]]
local function getValuableCount()
    local count = 0
    for key, name in pairs(slotTypes) do
        if isValuable(name) then
            count = count + 1
        end
    end
    return count
end

--[[
    Returns true if a table contains a specific value
]]
local function containsValue(table, value)
    for _, val in pairs(table) do
        if val == value then return true end
    end
    return false
end

--[[
    Returns a table which contains all slot indexes of types that appear in stored slots
]]
local function getSlotsForTypes(types)
    local slots = {}

    for key, name in pairs(slotTypes) do
        if containsValue(types, name) then
            local i = key + 2
            table.insert(slots, i, key)
        end
    end
    return slots
end

--[[
    Returns slots which are stored as empty but really aren't. (3rd slot as 1st, 1 - 14)
]]
local function getSlotTypeDifference()
    local differentSlots = {}

    for key, name in pairs(slotTypes) do
        local i = key + 2

        if turtle.getItemCount(i) > 0 and name == "empty" then
            table.insert(differentSlots, i, key)
        end
    end
    return differentSlots
end


--[[
    Adds the item types with the given typeValue from wasteTable to table typeTable. 

    wasteTable -- table to search for wasted items (ground, ores or valuables)
]]
local function addWasteTypesInTable(typeTable, wasteTable, typeValue)
    for name, val in pairs(wasteTable) do
        if val == typeValue then
            table.insert(typeTable, name)
        end
    end
end

--[[
    Returns all types below or equal to a given typeValue
]]
local function getWasteTypes(typeValue)
    local types = {}

    addWasteTypesInTable(types, ground, typeValue)
    if typeValue <= 3 then return types end

    addWasteTypesInTable(types, ores, typeValue)
    if typeValue <= 5 then return types end

    addWasteTypesInTable(types, valuables, typeValue)
    return types
end

--[[
    Tries to make a given slot empty by transfering the items to previous slots
]]
local function organizeSlot(slot)
    selectSlot(slot)
    local name = slotTypes[slot - 2]

    for i = 3, slot - 1 do
        local key = i - 2
        if turtle.getItemCount(i) < 64 and (slotTypes[key] == "empty" or slotTypes[key] == name) and turtle.transferTo(i) then
            slotTypes[key] = name
            saveSetting("SLOT_TYPES", slotTypes)

            if turtle.getItemCount(slot) > 0 then
                organizeSlot(slot)
            else
                slotTypes[slot - 2] = "empty"
                saveSetting("SLOT_TYPES", slotTypes)
            end
            return
        end
    end
end


--[[
    Drops all items from given slots except coal is used as a fuel.
]]
local function dropWaste(slots)
    for i, key in pairs(slots) do
        turtle.select(i)

        if slotTypes[key] == "minecraft:coal_ore" then
            turtle.refuel()
        else
            if not turtle.dropDown() then turtle.drop() end
        end

        slotTypes[key] = "empty"
        saveSetting("SLOT_TYPES", slotTypes)
    end
end

--[[
    Makes empty slots by dropping waste and combining slots.
    Items are dropped in the typeValue order (the least valuable first).
]]
local function handleFullInventory()
    local fullCount = 0

    while isFullInventory(fullCount) do
        local wasteSlots = getSlotsForTypes(getWasteTypes(fullCount))
        fullCount = fullCount + 1

        dropWaste(wasteSlots)

        for i = 16, 10, -1 do
            organizeSlot(i)
        end
    end
    selectSlot(1)
end




--[[
    Updates the saved slotTypes to match the current items in the inventory.
    New slots that don't yet have a type get a new type "name". Metadata is used to differentiate stone variants.
]]
local function updateSlotType(name, metadata)
    local differentSlots = getSlotTypeDifference()

    if name == "minecraft:stone" then
        if metadata == 1 then name = "granite" end
        if metadata == 3 then name = "diorite" end
        if metadata == 5 then name = "andesite" end
    end

    if name == "minecraft:stone" then name = "minecraft:cobblestone" end

    for i, key in pairs(differentSlots) do -- i is real slot number, key is i - 2
        if ground[name] ~= nil or isOre(name) then -- is ore or ground material
            if name == "minecraft:gravel" then
                slotTypes[key] = getSlotDetail(i, 1)
            else
                slotTypes[key] = name
            end
        else
            slotTypes[key] = "unknown"
        end
        saveSetting("SLOT_TYPES", slotTypes)
    end

    handleFullInventory()
end






--[[
    Mines a single block in a specific direction if necessary.
    
    inspect function inspects the dig direction (returns the name and metadata of the block),
    dig function digs in that direction and place function places a block in that direction (right clicks).
    
    Place function is only used to refuel lava. If onlyOres is false, then all found lava is refueled.
    Otherwise found lava is refueled only if turtle's fuel level is less than limit-1000. (so for normal turtles less than 19000)
    
    If mustDig is true, then this block has to be mined, even if onlyOres is true, for the turtle to continue/move in that direction.
    If turtle cannot dig a block (end portal frame or bedrock) when mustDig is true, 
    then turtle waits and broadcasts "UNBREAKABLE" message with its coordinates every 30 seconds. (for the quarryviewer)
]]
local function mineBlock(mustDig, oresOnly, place, dig, inspect)
    local name, metadata = inspect()
    if name == nil then return end -- is air

    -- dig the block if it is part of the turtle tunnel (mustDig) or setting is to clear the whole area (not oresOnly)
    local leaveClear = mustDig or (not oresOnly)

    if isLavaSource(name, metadata) and (turtle.getFuelLevel() < turtle.getFuelLimit() - 1000 or (not onlyOres)) then
        selectSlot(1)
        place()
        turtle.refuel()
        return
    end

    if isChest(name) and mustDig then
        while turtle.suck() or turtle.suckDown() do
            updateSlotType("chestloot", 0)
        end
        local count = turtle.getItemCount(1)
        if count > 1 then turtle.dropDown(count - 1) end

    elseif name == "minecraft:end_portal_frame" or name == "minecraft:bedrock" then
        updatePos()

        if not mustDig then return end

        while true do
            inform({"UNBREAKABLE", {pos.x, pos.y, pos.z}})
            sleep(30)
        end
    end

    if leaveClear or isValuable(name) then
        dig()
        updateSlotType(name, metadata)

        -- Check and dig level y=3 block if level y=4 block is not bedrock. 
        -- Still misses some valuable ores under bedrock.
        if currentLevel == 1 and minHeight == 5 and dig == turtle.digDown and name ~= "minecraft:bedrock" then
            turtle.down()
            name, metadata = inspect()
            if isValuable(name) then
                dig()
                updateSlotType(name, metadata)
            end
            if not turtle.up() then
                turtle.digUp()
                updateSlotType("unknown", 0)
                turtle.up()
            end
        end

        if leaveClear then
            name, metadata = inspect()
            while name ~= nil and not isLiquid(name) do
                dig()
                updateSlotType(name, metadata)

                name, metadata = inspect()
            end
        end
    end
end


-- Uses the mineBlock function with the specific directional functions place, dig and inspect
local function mineBlockForward(mustDig) mineBlock(mustDig, true, turtle.place, turtle.dig, inspectName) end
local function mineBlockUp(mustDig) mineBlock(mustDig, onlyOres, turtle.placeUp, turtle.digUp, inspectNameUp) end
local function mineBlockDown(mustDig) mineBlock(mustDig, onlyOres, turtle.placeDown, turtle.digDown, inspectNameDown) end



--[[
    Tries to move one block in a direction with the given movement function.

    This function is not used the first time mining a block. The mining and attack functions here are secondary.
    For example handles digging gravel.

    Loops until moves successfully. If cannot move, then first attacks and if that did not help
    then mines with the given mining function.
]]
local function moveBlock(movement, attack, mining)
    while not movement() do
        if not attack() then
            mining(true)
        end
    end
end


-- Uses the moveBlock function with the specific directional functions movement, attack, mining
local function moveBlockForward() moveBlock(turtle.forward, turtle.attack, mineBlockForward) end
local function moveBlockUp() moveBlock(turtle.up, turtle.attackUp, mineBlockUp) end
local function moveBlockDown() moveBlock(turtle.down, turtle.attackDown, mineBlockDown) end




--[[
    Checks mining forward, up and down and mines if necessary. 
    Moves stepCount times with a move function that handles attacking mobs and digging gravel.
]]
local function mine(move, mustDigDown, mustDigForward, mustDigUp, stepCount)
    for i = 1, stepCount do
        mineBlockUp(mustDigUp)
        mineBlockForward(mustDigForward)
        mineBlockDown(mustDigDown)

        move()
    end
end


-- Uses the mine function to mine a specific number of steps/blocks in a direction,
-- with the specific mustDig values for different directions.
local function mineForward(stepCount) mine(moveBlockForward, false, true, false, stepCount) end
local function mineUp(stepCount) mine(moveBlockUp, false, false, true, stepCount) end
local function mineDown(stepCount) mine(moveBlockDown, true, false, false, stepCount) end

--[[
    Helper to mine up and down with the same function.
    if stepCount is positive then mines down, if negative then up.
]]
local function mineVertical(stepCount)
    if stepCount > 0 then
        mineDown(stepCount)
    else
        mineUp(-stepCount)
    end
end

--[[
    Moves stepCount number of blocks with the movement function

    Only checks mining in one direction (usually the movement direction) 
    unlike the mine function which checks mining in all directions.
]]
local function go(movement, mining, stepCount)
    for i = 1, stepCount do
        mining(true)
        movement()
    end
end

--[[
    Helper for the go function to go up and down with the same function call.
    if stepCount is positive then goes down, if negative then up.
]]
local function goVertical(stepCount)
    if stepCount > 0 then
        go(moveBlockDown, mineBlockDown, stepCount)
    else
        go(moveBlockUp, mineBlockUp, -stepCount)
    end
end


-- vertical distance to the starting position. Negative if turtle is below the chests (usually is)
local function heightToChest()
    updatePos()
    return pos.y - startPos.y
end


-- drops only ores from the inventory (into the chest in front of the turtle)
local function emptyOnlyOres()
    for i = 3, 15 do
        local key = i - 2
        if isOre(slotTypes[key]) then
            selectSlot(i)
            turtle.drop()
        end
    end
end

--[[
    Unloads items from the turtle's inventory into a chest in front of the turtle.
    If the block in front isn't a chest then a new chest is placed.
    
    If chestNumber is 0 then only ores are unloaded.
    
    Returns false if couldn't unload all items in this chest (is cancelled), else true.
]]
local function unload(chestNumber)
    local name, metadata = inspectName()

    local movedForward = false
    local cancelled = false

    if not isChest(name) then
        mineBlockForward(true)
        selectSlot(2)
        while not turtle.place() do 
            if not turtle.attack() then
                moveBlockForward()
                movedForward = true

                if isChest(inspectName()) then
                    break
                end

                mineBlockForward(true)
                selectSlot(2)
                while not turtle.place() do 
                    turtle.attack()
                    print("Trying to place a chest...")
                    sleep(3)
                end
                
                break
            end
        end
    end

    if chestNumber == 0 then 
        emptyOnlyOres()
        cancelled = true

    elseif not emptyInventory(3, dirs.forward) then 
        cancelled = true
    end


    if movedForward then 
        turtle.back() 
    end

    if cancelled then 
        return false 
    end

    -- set every storage slot as "empty"
    for key, value in pairs(slotTypes) do
        slotTypes[key] = "empty"
    end
    saveSetting("SLOT_TYPES", slotTypes)

    return true
end


--[[
    Unloads the items into chests if necessary and goes to the y level of the next iteration.

    If turtle has enough valuable items (>5 valuable slots) then it goes to unload the items in the chests.
    Otherwise only goes to the beginning y level of the next iteration.
]]
local function handleItemUnload(alwaysUnload)
    local isLastLevel = currentLevel == iterations * 2

    -- don't unload if not enough ores
    if getValuableCount() <= 5 and (not isLastLevel) and not alwaysUnload then
        mineUp(3)
        turn180()
        return
    end

    isUnloading = true
    saveSetting("UNLOADING", isUnloading)

    local chestDist = heightToChest()
    local unloadCount = 0
    goVertical(chestDist)

    while not unload(unloadCount) do
        unloadCount = unloadCount + 1
        moveBlockDown()

        updatePos()
    end

    isUnloading = false
    saveSetting("UNLOADING", isUnloading)

    turn180()

    if not isLastLevel then
        goVertical(-chestDist - 3 - unloadCount)
    end
end


--[[
    Goes to the starting y level of the first iteration
]]
local function gotoStartHeight()
    goVertical(pos.y - minHeight)
end



--[[
    Main function of the quarry

    Assumes that turtle is on the start height (default y=5) starts the iteration loop.
]]
local function quarry(startIteration)

    for i = startIteration, iterations do
        currentLevel = 2 * i - 1
        saveSetting("LEVEL", currentLevel)

        mineForward(length)
        mineUp(3)
        mineBlockForward(false)
        turn180()

        currentLevel = currentLevel + 1
        saveSetting("LEVEL", currentLevel)

        mineForward(length)

        -- unload if necessary
        handleItemUnload()
    end


    -- Finished
    
    turtle.up()

    isRunning = false
    saveSetting("RUNNING", isRunning)

    if isClone then
        left()
    end

    inform({"FINISHED", true})
end



if isDebug then
    --call a function to test
    return
end



if not isRunning then
    inform({"FINISHED", false})

    settings.set("START_POSITION", startPos)
    settings.set("SLOT_TYPES", slotTypes)
    settings.set("UNLOADING", isUnloading)
    settings.set("LEVEL", currentLevel)
    
    settings.set("HOST_ID", hostId)

    saveSetting("ROTATION", rotation)


    isRunning = true
    saveSetting("RUNNING", isRunning)

    gotoStartHeight()

    quarry(1)
end








-----------------------------------------------------------------------------------------------------------------------
-- Resuming from an unknown state
-----------------------------------------------------------------------------------------------------------------------

--[[
    Distance from the start (x,z) coordinate (ignoring y)
]]
local function distanceFromStart()
    local start = vector.new(startPos.x, startPos.y, startPos.z)
    local diff = pos - start

    local xDist = math.abs(diff.x)

    if xDist > 0 then return xDist else return math.abs(diff.z) end
end

local function gotoLevelHeight(nextIteration)
    if nextIteration ~= nil then
        local height = minHeight + (nextIteration - 1) * 6

        updatePos()
        mineVertical(pos.y - height)
        if rotation == rots.back then turn180() end
        return
    end

    local height = endHeight
    for i = minHeight + 3, endHeight, 6 do
        if i >= pos.y then
            height = i
            break
        end
    end

    mineVertical(pos.y - height)
    if rotation == rots.away then turn180() end
end



local function loadQuarrySettings()
    startPos = settings.get("START_POSITION")
    rotation = settings.get("ROTATION")

    currentLevel = settings.get("LEVEL")
    isUnloading = settings.get("UNLOADING")

    slotTypes = settings.get("SLOT_TYPES")
    hostId = settings.get("HOST_ID")
end

--[[
    Attempts to resume the quarry.

    Use carefully!!!

    There is a small possibility that a turtle will start in a wrong direction and cause a mess!!!
    E.g if a turtle is turning 180 when chunk reload happens it is possible that the wrong rotation gets saved.
]]
local function resume()
    loadCommonSettings()

    loadQuarrySettings()

    if hostId == os.getComputerID() then
        isClone = false
    end

    local startDist = distanceFromStart()
    local nextIteration = math.floor((currentLevel + 1) / 2) + 1

    if rotation == rots.left then rotation = rots.back saveSetting("ROTATION", rotation) left()
    elseif rotation == rots.right then rotation = rots.away saveSetting("ROTATION", rotation) left() end

    if pos.y == 4 then
        if not turtle.up() then
            turtle.digUp()
            updateSlotType("unknown", 0)
            turtle.up()
        end
        updatePos()
        startDist = distanceFromStart()
    end

    if startDist == 0 then
        if isUnloading then
            handleItemUnload(true)
        end

        if currentLevel < iterations * 2 then gotoLevelHeight(nextIteration) end

        quarry(nextIteration)
        return
    end

    if startDist == length then
        gotoLevelHeight()

        currentLevel = currentLevel + 1
        saveSetting("LEVEL", currentLevel)

    elseif rotation == rots.away then
        mineForward(length - startDist)
        mineUp(3)
        mineBlockForward(false)
        turn180()

        startDist = length

        currentLevel = currentLevel + 1
        saveSetting("LEVEL", currentLevel)
    end


    mineForward(startDist)

    -- go to chest and unload if necessary
    handleItemUnload()

    quarry(nextIteration)
end


if isRunning then resume() end
