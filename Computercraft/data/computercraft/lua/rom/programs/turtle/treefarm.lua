
local startZ = 1228
local startY = 73

local treeHeight = 6

local pos = nil


--[[
    Executes the movement function stepCount times.
    If movement() returns false then it executes the attack function until movement() returns true
]]
local function simpleMovement(stepCount, movement, attack)
    local c = 1
    while c <= stepCount do
        if movement() then
            c = c + 1
        else
            attack()
        end
    end
end


local function forward(stepCount) simpleMovement(stepCount, turtle.forward, turtle.attack) end
local function back(stepCount) simpleMovement(stepCount, turtle.back, turtle.attack) end
local function up(stepCount) simpleMovement(stepCount, turtle.up, turtle.attack) end
local function down(stepCount) simpleMovement(stepCount, turtle.down, turtle.attack) end


--[[
    Empties turtle's inventory (slots {fromSlot..16}) so if fromSlot is 1 the whole inventory is emptied

    Tries to drop items in the direction dir (default down). If the items cannot be dropped then returns false.
    Selects slot 1 after successfully emptying inventory.
]]
local function emptyInventory()
    for i = 3, 16 do
        if turtle.getItemCount(i) > 0 then
            turtle.select(i)

            if not turtle.drop() then return false end
        end
    end
    turtle.select(1)
    return true
end

local function openRednet()

    -- Find a modem (code taken from gps api)
    local sModemSide = nil
    for n,sSide in ipairs(rs.getSides()) do
        if peripheral.getType(sSide) == "modem" then
            sModemSide = sSide
            break
        end
    end

    if sModemSide == nil then
        print("No wireless modem attached")
        return false
    end

    modemSide = sModemSide

    rednet.open(modemSide)
    return true
end

--[[
    Updates the global pos vector variable with the turtle's position from gps
    If gps.locate cannot locate turtle then returns false and the pos vector is reset to (nil,nil,nil)
]]
local function updatePos()
    local x, y, z = gps.locate()

    if x == nil then
        print("Could not get turtle position!\nCheck that turtle is in range of a gps host")
        pos = vector.new(nil, nil, nil)
        return false
    end

    pos = vector.new(x, y, z)
    return true
end



local function refuel()
    turtle.suckDown()
    if turtle.getItemCount() > 32 then turtle.refuel(turtle.getItemCount() - 32) end

    turtle.select(2)
    turtle.suckDown()
    turtle.refuel(turtle.getItemCount() - 1)

    turtle.select(1)
end



if not openRednet() then return end
if not updatePos() then return end


if pos.y < startY then
    up(startY - pos.y)
else
    down(pos.y - startY)
    back(pos.z - startZ)
end

while true do
    for i = 1, 4 do
        forward(4)
        turtle.dig()
        turtle.forward()
        for j = 1, treeHeight do
            turtle.dig()
            turtle.digUp()
            turtle.up()
        end
        down(treeHeight)
    end
    forward(4)
    emptyInventory()
    for i = 1, 4 do
        back(5)
        turtle.place()
    end
    back(4)
    
    for i = 1, 18 do
        print(i .. "/18")
        sleep(60)
    end
    
    down(5)
    refuel()
    up(5)
end


