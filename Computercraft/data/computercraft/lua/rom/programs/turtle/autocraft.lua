local function suckToSlot(i)
    turtle.select(i)
    while turtle.getItemCount() == 0 do 
        turtle.suck(8)
    end 
end


local args = {...}

local type = "vr" -- vertical row or "b" block
local craftCount = 2

if args[1] ~= nil then type = args[1] end
if args[2] ~= nil then craftCount = tonumber(args[2]) end



local function suckPattern()
    if type == "vr" then --vertical row
        for i = 4, 12 do
            if i % 4 == 0 then
                suckToSlot(i)
            end
        end 
    elseif type == "b" then --block
        for i = 5, 15 do
            if i % 4 ~= 0 then
                suckToSlot(i)
            end
        end 
    end
end

local function main()
    suckPattern()

    turtle.select(1)

    for i = 1, craftCount do
        if turtle.craft() then
            while not turtle.dropDown() do end
        end
    end
end

turtle.select(1)
while turtle.getItemCount() > 0 and (not turtle.dropDown()) do end

while true do main() end