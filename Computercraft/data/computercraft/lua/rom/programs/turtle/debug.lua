local args = {...}

local slotTypes = {
--[["minecraft:bucket", "minecraft:chest",]]"minecraft:redstone_ore",            "empty", 
    "minecraft:stone",  "minecraft:redstone_ore",            "minecraft:stone",            "empty", 
    "empty",            "empty",            "empty",            "empty", 
    "empty",            "empty",            "minecraft:stone",            "minecraft:redstone_ore", 
}

local function organizeSlot(slot)
    slot = tonumber(slot)
    turtle.select(slot)
    local name = slotTypes[slot - 2]

    for i = 3, slot - 1 do
        local key = i - 2
        if turtle.getItemCount(i) < 64 and (slotTypes[key] == "empty" or slotTypes[key] == name) and turtle.transferTo(i) then
            slotTypes[key] = name

            if turtle.getItemCount(slot) > 0 then 
                organizeSlot(slot, name)
            else
                slotTypes[slot - 2] = "empty"
            end
            return
        end
    end
end


organizeSlot(args[1])

print(slotTypes[13])