local args = {...}

local x = args[1]
local y = args[2]

for i = 1, y do
    for j = 1, x do

        if j ~= 1 then turtle.forward() end


        local downCount = 0;
        local suc, data = turtle.inspectDown()

        while data.name == "minecraft:sand" or not suc do
            turtle.digDown()
            turtle.down()
            downCount = downCount + 1

            suc, data = turtle.inspectDown()
        end

        for a = 1, downCount do
            turtle.up()
        end
    end
    
    if i % 2 == 0 then turtle.turnLeft() else turtle.turnRight() end
    turtle.forward()
    if i % 2 == 0 then turtle.turnLeft() else turtle.turnRight() end
end

