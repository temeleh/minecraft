local protocol = "quarry"

local rots = {left = 0, right = 1, away = 2, back = 3}

rednet.open("back")


local view = {}
local scrollPos = 0

local viewLength = 0

local function cls() 
    term.clear() 
    term.setCursorPos(1, 1)

    term.blit("ID  | LEV | DIR  | SPACE  ", "ffffffffffffffffffffffffff", "00000000000000000000000000")

    print("\n")
end

local function emptySlots(types) 
    local count = 0

    for key, value in pairs(types) do
        if value == "empty" then
            count = count + 1
        end
    end

    return count
end

local function drawView()
    cls()

    local lineNum = 0

    for i, data in pairs(view) do

        if lineNum >= scrollPos and lineNum - scrollPos < 17 then
            local x, y = term.getCursorPos()

            write(i)
            term.setCursorPos(5, y)
            write("| ")

            if data["FINISHED"] then
                write("FINISHED")

            elseif data["UNBREAKABLE"] ~= nil then
                local u = data["UNBREAKABLE"]

                write("UNBREAKABLE\n" .. u[1] .. ", " .. u[2] .. ", " .. u[3] .. "\n")

            elseif data["UNLOADING"] then
                write("UNLOADING")

            else
                local level = data["LEVEL"]
                local dir = data["ROTATION"]
                local space = data["SLOT_TYPES"]

                if level == nil then level = "-" end
                if space == nil then space = "-" else space = emptySlots(space) end

                if dir == nil then dir = "-   "
                elseif dir == 0 or dir == 1 then dir = "TURN"
                elseif dir == 2 then dir = "AWAY"
                else dir = "BACK" end

                write(level)
                term.setCursorPos(11, y)
                write("| " .. dir .. " | " .. space)
            end

            print()
        end

        lineNum = lineNum + 1
    end

    viewLength = lineNum
end

local function updateView(id, setting, value)
    if view[id] == nil then view[id] = {} end
    view[id][setting] = value

    drawView()
end

local function receiveUpdate()
    while true do
        local id, data = rednet.receive(protocol)

        updateView(id, data[1], data[2])
    end
end

local function scroll()
    while true do
        local event, key = os.pullEvent("key")

        if key == keys.up then scrollPos = scrollPos - 1
        elseif key == keys.down then scrollPos = scrollPos + 1 end
    
        if scrollPos > viewLength - 17 then scrollPos = scrollPos - 1 
        elseif scrollPos < 0 then scrollPos = 0 end
    
        drawView()
    end
end

cls()

parallel.waitForAny(receiveUpdate, scroll)
